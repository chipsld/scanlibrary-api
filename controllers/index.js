const express = require('express');
const router = express.Router();


router.use('/manga', require('./manga'));
// router.use('/user', require('./user'));


router.get('/', function(req, res) {
    res.send('Accueil API Scanlibrary');
});


module.exports = router ;