const express = require('express');
const router = express.Router();
const Manga = require('../models/manga');


router.get('/all', function(req,res) {
    Manga.getAll(req,res);
});

router.get('/collection/:userid', function(req,res) {
    Manga.getCollection(req,res);
});

router.post('/addCollection', function(req,res) {
    Manga.addMangaInCollection(req,res);
});

router.put('/addManga', function(req,res) {
    Manga.add(req,res);
});

router.post('/updateLow' , function (req,res) {
    Manga.updateLow(req,res);
});

router.post('/updateUp' , function (req,res) {
    Manga.updateUp(req,res);
});


module.exports = router ;