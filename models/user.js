const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    id_user : String,
    username: String,
    password: String,
    admin: Boolean,
    langage : String
},
{
	collection : 'user'
},
{
	versionKey: false 
});

var User = mongoose.model('User', userSchema);


exports.getUser = function(req,res)
{
	let user = req.params.username;
	let pwd = req.params.password;

	let query = User.where({username: user},{password: pwd});

	query.findOne()
	.exec()
	.then(docs => {
		res.status(200).json(docs);
	})
	.catch(err => console.log(err));
}



// GET A USERID

router.post('/userid',function(req,res)
{
	let user = req.body._id;

	User.find({_id:user})
	.exec()
	.then(docs => {
		res.status(200).json(docs);
	})
	.catch(err => console.log(err));
})


/**
 * Modification d'un profil utilisateur.
 * @param {number} userid - Numéro de l'utilisateur
 */

router.post('/profil', function(req,res) {

	let userid = req.body.userid;
	let query = User.where({ _id: userid });

	query.update({ langage : req.body.langage , username : req.body.username , password : req.body.password })
	.exec()
	.then(docs => {
		res.status(200).json(docs);
	})
	.catch(err => console.log(err));
});




/**
 * Récupération d'un profil utilisateur.
  * @param {number} userid - Numéro de l'utilisateur connecté
 */

router.get('/profil', function(req,res) {
	
	let userid = req.body.userid;
	let query = User.where({ _id: userid });

	query.findOne()
	.exec()
	.then(docs => {
		res.status(200).json(docs);
	})
	.catch(err => console.log(err));

});
