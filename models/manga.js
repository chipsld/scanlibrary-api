const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const mangaSchema = new Schema({
	_id: mongoose.Schema.Types.ObjectId,
    id_manga: Number,
    name: String,
    cover: String,
    link: String,
    inCollection : Array,
    tag: Number
    },
    {
        collection : 'manga'
    },
    {
        versionKey: false 
    }
);


var Manga = mongoose.model('Manga', mangaSchema);


//* Get all mangas in the database

exports.getAll = function(req,res) {

    Manga.find().sort('name')
    .exec()
    .then(docs => {
        res.status(200).json(docs);
    })
    .catch(err => {
        res.status(500).json({
            error: err
        })
    })
}


exports.getCollection = function(req,res)
{
  
    let userid = req.params.userid;
    let query = Manga.where({'inCollection' : { $elemMatch : { id_user : userid }}});
    
    console.log(query);
	query.find().sort('name')
	.exec()
	.then(docs => {
        res.status(200).json(docs);
		// console.log(docs);
	})
	.catch(err => {
		res.status(500).json({
			error: err
		})
    })
    
}

//* Add a manga in a user's collection

exports.addMangaInCollection = function(req,res)
{
    let userid = req.body.userid;
    let mangaid = req.body.mangaid;
    
    var collection = { id_user: userid, read: 0 };

    let query = Manga.findOneAndUpdate({ _id : mangaid },{ $push: { inCollection : collection}});
    query
    .exec()
    .then(docs => {
        res.status(200).json(docs);
        console.log(docs);
    })
    .catch(err => {
        res.status(500).json({
            error: err
        });
        console.log(err);
    })
}




exports.add = function(req,res)
{

	const manga = new Manga({
		_id: new mongoose.Types.ObjectId(),
		id_manga: req.body.id_manga,
	    name: req.body.name,
	    cover: req.body.cover,
		link: req.body.link,
		inCollection : [],
		tag: req.body.tag
	});

	manga
	.save()
	.then(result => {
		console.log("Creation reussie");
	})
	.catch(err => console.log(err));


}



//  * Augmente le nombre de chapitre lu pour un manga dans 
//  * la collection d'un utilisateur.
//  * @param {number} userid - Numéro de l'utilisateur
//  * @param {number} mangaid - Numéro du manga
//  */


exports.updateUp = function(req,res)
{

	let userid = req.body.userid;
	let mangaid = req.body.mangaid;

	Manga.findOneAndUpdate({ _id : mangaid , 'inCollection' : { $elemMatch : { id_user : userid }}},
						   { $inc : { "inCollection.$.read" : 1}}, {new : true}, (err, newread) => {
										if (err) return res.status(500).send(err);
										res.send(newread);
							});

}




//  * Diminue le nombre de chapitre lu pour un manga dans 
//  * la collection d'un utilisateur.
//  * @param {number} userid - Numéro de l'utilisateur
//  * @param {number} mangaid - Numéro du manga
//  */


exports.updateLow = function(req,res)
{
	let userid = req.body.userid;
	let mangaid = req.body.mangaid;

	Manga.findOneAndUpdate({ _id : mangaid , 'inCollection' : { $elemMatch : { id_user : userid }}},
						   { $inc : { "inCollection.$.read" : -1}}, {new : true}, (err, newread) => {
								if (err) return res.status(500).send(err);
								res.send(newread);
							});

}
