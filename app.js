const express = require('express');
const helmet = require('helmet');
const app = express();
const bodyParser = require('body-parser');
const connection = require('./connexion');
const router = express.Router()

app.use(helmet());
app.use(router);
app.use(bodyParser.urlencoded({ extended: true , useUnifiedTopology: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
  });


// app.use(require('./middlewares'));
app.use(require('./controllers'));


let port = process.env.PORT || 8081;
connection.init();
app.listen(port);







